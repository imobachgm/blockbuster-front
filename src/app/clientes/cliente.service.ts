import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Cliente } from './cliente';
import { CLIENTES } from './clientes.json';
import { Observable, of, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators'
import { AlertService } from '../alert/alert.service';
 
@Injectable()
export class ClienteService {

  urlServer: string = 'http://localhost:8090/';

  constructor(private http: HttpClient, private alertService: AlertService) { }

  getClientes(): Observable<Cliente[]> {
    // return of(CLIENTES); 
    return this.http.get<Cliente[]>(this.urlServer + 'clientes').pipe(  // Le acoplamos el EndPoint.
      catchError(error => {
        // console.log("Algo pasó");
        console.error(`getClientes error: "${error.message}"`); // Las comillas inclinadas son para interpolación (incorporar variables dentro).
        this.alertService.error(`Error al consultar clientes: "${error.message}"`) // Posible funcionalidad: , {autoClose: true}.
        return throwError(error) // Porque el pipe es un observable y espera que le llegue un observable. throwError devuelve un observable vacío. propaga el error hacia arriba, por si lo quisiéramos gestionar anteriormente.
      })
    );
  }
}
