import { Cliente } from './cliente';

 export const CLIENTES: Cliente[] = [
    {idCliente: 1, nombreCliente: 'Andrés', fechaNacimiento: '20/10/1994', correo: 'andres@atos.net',dni: '12345678A'},
    {idCliente: 2, nombreCliente: 'Belén', fechaNacimiento: '21/10/1994', correo: 'belen@atos.net',dni: '12345678B'},
    {idCliente: 3, nombreCliente: 'Jorge', fechaNacimiento: '22/10/1994', correo: 'jorge@atos.net',dni: '12345678C'},
    {idCliente: 4, nombreCliente: 'Laura', fechaNacimiento: '23/10/1994', correo: 'laura@atos.net',dni: '12345678D'}
 ];