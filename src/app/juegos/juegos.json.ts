import { Juego } from './juego';

export const JUEGOS: Juego[] = [
      {idJuego: 1, titulo: 'Juego1', fechaLanzamiento: '10/10/2010', precio: 25, pegi: 12, categoria: 'Categoria1'},
      {idJuego: 2, titulo: 'Juego2', fechaLanzamiento: '11/10/2010', precio: 30, pegi: 14, categoria: 'Categoria2'},
      {idJuego: 3, titulo: 'Juego3', fechaLanzamiento: '12/10/2010', precio: 35, pegi: 16, categoria: 'Categoria3'},
      {idJuego: 4, titulo: 'Juego4', fechaLanzamiento: '13/10/2010', precio: 40, pegi: 18, categoria: 'Categoria4'}
    ];