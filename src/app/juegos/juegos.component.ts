import { Component, OnInit } from '@angular/core';
import { Juego } from './juego';
import { JUEGOS } from './juegos.json';

@Component({
  selector: 'app-juegos',
  templateUrl: './juegos.component.html',
  styleUrls: ['./juegos.component.css']
})
export class JuegosComponent implements OnInit {
  
  juegos: Juego[];

  showId: boolean = false; // Si no ponemos nada, por defecto será false.

  constructor() { }

  ngOnInit(): void {
    this.juegos = JUEGOS;
  }
    
  switchId(): void {
    this.showId = !this.showId;
  }
}
